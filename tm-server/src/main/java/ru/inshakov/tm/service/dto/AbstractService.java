package ru.inshakov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.dto.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
