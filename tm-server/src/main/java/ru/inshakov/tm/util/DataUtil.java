package ru.inshakov.tm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DataUtil {

    static String convertToString(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date);
    }

    static String getDate() {
        return convertToString(new Date());
    }
}
