package ru.inshakov.tm.api.entity;

public interface IWBS extends IHasStartDate, IHasName, IHasCreated, IHasStatus {
}