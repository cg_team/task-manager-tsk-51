package ru.inshakov.tm.api.repository.model;

import ru.inshakov.tm.api.IGraphRepository;
import ru.inshakov.tm.model.UserGraph;

public interface IUserGraphRepository extends IGraphRepository<UserGraph> {

    UserGraph findByLogin(final String login);

    UserGraph findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final UserGraph userGraph);

}
