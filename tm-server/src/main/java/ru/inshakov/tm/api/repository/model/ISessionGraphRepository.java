package ru.inshakov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IGraphRepository;
import ru.inshakov.tm.model.SessionGraph;

import java.util.List;

public interface ISessionGraphRepository extends IGraphRepository<SessionGraph> {

    List<SessionGraph> findAllByUserId(@Nullable String userId);

    void removeByUserId(@Nullable String userId);

    void update(final SessionGraph sessionGraph);

}
