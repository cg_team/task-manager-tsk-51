package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.service.dto.*;

public interface ServiceLocator {

    @NotNull
    ITaskService getTaskDtoService();

    @NotNull
    IProjectService getProjectDtoService();

    @NotNull
    IProjectTaskService getProjectTaskDtoService();

    @NotNull
    IUserService getUserDtoService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionService getSessionDtoService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IConnectionService getConnectionService();
}
