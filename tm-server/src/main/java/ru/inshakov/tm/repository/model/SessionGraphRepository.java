package ru.inshakov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.model.ISessionGraphRepository;
import ru.inshakov.tm.model.SessionGraph;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionGraphRepository extends AbstractGraphRepository<SessionGraph> implements ISessionGraphRepository {

    public SessionGraphRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public SessionGraph getReference(@NotNull final String id) {
        return entityManager.getReference(SessionGraph.class, id);
    }

    @Override
    public List<SessionGraph> findAllByUserId(String userId) {
        return entityManager
                .createQuery("SELECT e FROM SessionGraph e WHERE e.user.id = :userId", SessionGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(String userId) {
        entityManager
                .createQuery("DELETE FROM SessionGraph e WHERE e.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    public List<SessionGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionGraph e", SessionGraph.class).getResultList();
    }

    public SessionGraph findById(@Nullable final String id) {
        return entityManager.find(SessionGraph.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionGraph e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        SessionGraph reference = entityManager.getReference(SessionGraph.class, id);
        entityManager.remove(reference);
    }
}