package ru.inshakov.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.model.IUserGraphRepository;
import ru.inshakov.tm.model.UserGraph;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserGraphRepository extends AbstractGraphRepository<UserGraph> implements IUserGraphRepository {

    public UserGraphRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    public UserGraph getReference(@NotNull final String id) {
        return entityManager.getReference(UserGraph.class, id);
    }

    @NotNull
    public List<UserGraph> findAll() {
        return entityManager.createQuery("SELECT e FROM UserGraph e", UserGraph.class).getResultList();
    }

    public UserGraph findById(@Nullable final String id) {
        return entityManager.find(UserGraph.class, id);
    }

    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserGraph e")
                .executeUpdate();
    }

    public void removeById(@Nullable final String id) {
        UserGraph reference = entityManager.getReference(UserGraph.class, id);
        entityManager.remove(reference);
    }

    @Nullable
    @Override
    public UserGraph findByLogin(@Nullable final String login) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM UserGraph e WHERE e.login = :login", UserGraph.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("login", login)
                        .setMaxResults(1)
        );
    }

    @Nullable
    @Override
    public UserGraph findByEmail(@Nullable final String email) {
        return getSingleResult(
                entityManager
                        .createQuery("SELECT e FROM UserGraph e WHERE e.email = :email", UserGraph.class)
                        .setHint(QueryHints.HINT_CACHEABLE, true)
                        .setParameter("email", email)
                        .setMaxResults(1)
        );
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM ProjectGraph e WHERE e.login = :login")
                .setParameter(login, login)
                .executeUpdate();
    }

}