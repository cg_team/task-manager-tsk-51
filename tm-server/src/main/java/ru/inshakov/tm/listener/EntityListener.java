package ru.inshakov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.bootstrap.Bootstrap;
import ru.inshakov.tm.enumerated.EntityOperationType;

import javax.persistence.*;

import static ru.inshakov.tm.enumerated.EntityOperationType.*;

public class EntityListener {

    @PostLoad
    public void onPostLoad(@Nullable final Object record) {
        sendMessage(record, LOAD);
    }

    @PrePersist
    public void onPrePersist(@Nullable final Object record) {
        sendMessage(record, START_PERSIST);
    }

    @PostPersist
    public void onPostPersist(@Nullable final Object record) {
        sendMessage(record, FINISH_PERSIST);
    }

    @PreUpdate
    public void onPreUpdate(@Nullable final Object record) {
        sendMessage(record, START_UPDATE);
    }

    @PostUpdate
    public void onPostUpdate(@Nullable final Object record) {
        sendMessage(record, FINISH_UPDATE);
    }

    @PreRemove
    public void onPreRemove(@Nullable final Object record) {
        sendMessage(record, START_REMOVE);
    }

    @PostRemove
    public void onPostRemove(@Nullable final Object record) {
        sendMessage(record, FINISH_REMOVE);
    }

    @SneakyThrows
    private void sendMessage(
            @Nullable final Object record,
            @NotNull final EntityOperationType type
    ) {
        Bootstrap.sendMessage(record, type.toString());
    }

}
