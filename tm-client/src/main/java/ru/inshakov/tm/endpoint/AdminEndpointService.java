
package ru.inshakov.tm.endpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.*;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "AdminEndpointService", targetNamespace = "http://endpoint.tm.inshakov.ru/", wsdlLocation = "http://localhost:8080/AdminEndpoint?WSDL")
public class AdminEndpointService
    extends Service
{

    private final static URL ADMINENDPOINTSERVICE_WSDL_LOCATION;
    private final static WebServiceException ADMINENDPOINTSERVICE_EXCEPTION;
    private final static QName ADMINENDPOINTSERVICE_QNAME = new QName("http://endpoint.tm.inshakov.ru/", "AdminEndpointService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/AdminEndpoint?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ADMINENDPOINTSERVICE_WSDL_LOCATION = url;
        ADMINENDPOINTSERVICE_EXCEPTION = e;
    }

    public AdminEndpointService() {
        super(__getWsdlLocation(), ADMINENDPOINTSERVICE_QNAME);
    }

    public AdminEndpointService(WebServiceFeature... features) {
        super(__getWsdlLocation(), ADMINENDPOINTSERVICE_QNAME, features);
    }

    public AdminEndpointService(URL wsdlLocation) {
        super(wsdlLocation, ADMINENDPOINTSERVICE_QNAME);
    }

    public AdminEndpointService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ADMINENDPOINTSERVICE_QNAME, features);
    }

    public AdminEndpointService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public AdminEndpointService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns AdminEndpoint
     */
    @WebEndpoint(name = "AdminEndpointPort")
    public AdminEndpoint getAdminEndpointPort() {
        return super.getPort(new QName("http://endpoint.tm.inshakov.ru/", "AdminEndpointPort"), AdminEndpoint.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns AdminEndpoint
     */
    @WebEndpoint(name = "AdminEndpointPort")
    public AdminEndpoint getAdminEndpointPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://endpoint.tm.inshakov.ru/", "AdminEndpointPort"), AdminEndpoint.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ADMINENDPOINTSERVICE_EXCEPTION!= null) {
            throw ADMINENDPOINTSERVICE_EXCEPTION;
        }
        return ADMINENDPOINTSERVICE_WSDL_LOCATION;
    }

}
