package ru.inshakov.tm.command.admin;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.util.TerminalUtil;

public final class UserRemoveByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "userGraph-remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "remove userGraph by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().removeByLogin(getSession(), login);
    }
}
