package ru.inshakov.tm.command.task;

import ru.inshakov.tm.command.TaskAbstractCommand;

public class TaskClearCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all taskGraphs.";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskEndpoint().clearTask(getSession());
    }
}
