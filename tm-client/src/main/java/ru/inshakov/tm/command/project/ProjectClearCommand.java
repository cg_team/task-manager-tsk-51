package ru.inshakov.tm.command.project;

import ru.inshakov.tm.command.ProjectAbstractCommand;

public class ProjectClearCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "projectGraph-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projectGraphs.";
    }

    @Override
    public void execute() {
        serviceLocator.getProjectEndpoint().clearProject(getSession());
    }
}
