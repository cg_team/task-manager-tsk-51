package ru.inshakov.tm.command.auth;

import ru.inshakov.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout from to application.";
    }

    @Override
    public void execute() {
        serviceLocator.getSessionEndpoint().close(getSession());
    }
}
