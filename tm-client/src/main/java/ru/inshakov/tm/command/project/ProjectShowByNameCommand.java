package ru.inshakov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.ProjectAbstractCommand;
import ru.inshakov.tm.endpoint.Project;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends ProjectAbstractCommand {
    @Override
    public String name() {
        return "projectGraph-show-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show projectGraph by name.";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectByName(getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        show(project);
    }
}
