package ru.inshakov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class IndexIncorrectException extends RuntimeException {

    public IndexIncorrectException(@Nullable final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
